import AppGenerator from 'generator-jhipster/esm/generators/app';
import {
  PRIORITY_PREFIX,
  INITIALIZING_PRIORITY,
  END_PRIORITY,
  COMPOSING_PRIORITY
} from 'generator-jhipster/esm/priorities';
import { ANGULAR, blueprints, DEVOPS_BLUEPRINT } from '../constants.mjs';

export default class extends AppGenerator {
  constructor(args, opts, features) {
    super(args, opts, { ...features, taskPrefix: PRIORITY_PREFIX, sbsBlueprint: true });

    this.jhipsterOptions({
      executeBlueprint: {
        desc: 'Sub Blueprint',
        type: String
      }
    });

    this.sbsBlueprint = true;
  }

  get [INITIALIZING_PRIORITY]() {
    return {
      async initializingTemplateTask() {
        this.executeBlueprint = ANGULAR;
      }
    };
  }

  get [COMPOSING_PRIORITY]() {
    return {
      async composingTask() {
        blueprints.forEach(blueprint => {
          if (this.executeBlueprint === blueprint.value) {
            this.composeWithJHipster(`${DEVOPS_BLUEPRINT}:${blueprint.value}`)
          }
        })
      }
    }
  }
}
