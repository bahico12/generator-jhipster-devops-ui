import { CLIENT_TEMPLATES_APP_DIR, MAIN_DIR } from '../constants.mjs';

export const replaceEntityFilePath = (data, filepath) =>
  filepath
    .replace(/_entityFolder/, data.entityFolderName)
    .replace(/_entityFile/, data.entityFileName)
    .replace(/_entityModel/, data.entityModelFileName);

export const appRenameTo = (data, filepath) => `${data.clientSrcDir}app/${replaceEntityFilePath(data, filepath)}`;
export const mainRenameTo = (data, filepath) => `${data.clientSrcDir}${replaceEntityFilePath(data, filepath)}`;


export const clientApplicationBlock = {
  renameTo: appRenameTo,
  path: CLIENT_TEMPLATES_APP_DIR,
};

export const clientApplicationMain = {
  renameTo: mainRenameTo,
  path: MAIN_DIR
};
