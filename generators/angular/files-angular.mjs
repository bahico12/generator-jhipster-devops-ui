import { clientApplicationBlock, clientApplicationMain } from '../client/utils.mjs';

export const files = {
  common: [
    {
      templates: [
        "angular.json",
        "tsconfig.json"
      ]
    }
  ],
  angularMain: [
    {
      ...clientApplicationBlock,
      templates: [
        // entities
        "entities/entity-navbar-items.ts",
        "admin/sidebar.items.ts",
        "layouts/main/main.component.ts",
        "layouts/main/main.component.html",
        "layouts/sidebar/sidebar.component.html",
        "layouts/sidebar/sidebar.component.scss",
        "layouts/sidebar/sidebar.component.ts",
        "layouts/sidebar/sidebar.route.ts",
        "layouts/sidebar/sidebar-item.model.ts",
        "layouts/profiles/profile.service.ts",
        "layouts/navbar/navbar.component.html",
        "layouts/navbar/navbar.component.scss",

        // home
        'home/home.component.html',
        'home/home.component.scss',

        // app
        "app-routing.module.ts",
        "app.module.ts",
      ]
    },
    {
      ...clientApplicationMain,
      templates: [
        'favicon.ico',
        'content/images/DEVOPS logo1.svg',
        // scss
        'content/scss/global.scss',
        'content/scss/_bootstrap-variables.scss',
        'content/scss/ng-zorro/ng-zorro-overrides.scss',
        'content/scss/ng-zorro/nz-switch.scss',
      ]
    }
  ],
  angularShared: [
    {
      ...clientApplicationBlock,
      templates: [
        'shared/functions.ts',
        // decorators
        'shared/decorators/get-value.ts',

        // components
        //alert
        'shared/component/bm-alert/bm-alert.component.ts',
        'shared/component/bm-alert/bm-alert.model.ts',
        'shared/component/bm-alert/bm-alert.service.ts',
        // array
        'shared/component/bm-array/bm-array.component.ts',
        'shared/component/bm-array/bm-array.component.html',
        // amount
        'shared/component/bm-amount/bm-amount.module.ts',
        'shared/component/bm-amount/bm-amount.pipe.ts',
        'shared/component/bm-amount/bm-amount.service.ts',
        'shared/component/bm-amount/bm-amount-input.component.ts',
        'shared/component/bm-amount/bm-amount-input.template.html',
        // file
        'shared/component/bm-file/bm-file.component.ts',
        'shared/component/bm-file/bm-file.template.html',
        'shared/component/bm-file/bm-file.model.ts',
        'shared/component/bm-file/bm-file.module.ts',
        'shared/component/bm-file/bm-table-image-viewer.component.ts',
        'shared/component/bm-file/bm-table-image-viewer.template.html',
        // json-viewer
        'shared/component/bm-json-viewer/bm-json-viewer.component.ts',
        'shared/component/bm-json-viewer/bm-json-viewer.style.scss',
        // select-input
        'shared/component/select-input/bm-select-input.component.ts',
        'shared/component/select-input/bm-select-input.module.ts',
        'shared/component/select-input/bm-select-input.template.html',

        // directive
        'shared/directive/bm-boolean-badge.directive.ts',
        'shared/directive/element-resolver.directive.ts',
        'shared/directive/input-validator/input-validator.directive.ts',
        'shared/directive/input-validator/input-validator.module.ts',
        'shared/directive/input-validator/input-validator-button.directive.ts',

        // pipe
        'shared/pipe/bm-date.pipe.ts',

        // abstract classes
        'shared/entity/entity.service.ts',
        'shared/entity/entity-base.service.ts',
        'shared/entity/entity-base-component.ts',
        'shared/entity/entity-update-base-component.ts',
        'shared/entity/entity-config.model.ts',
        'shared/entity/hooks/base-component-hooks.ts',

        // entity crud

        //              AUDIT
        'shared/entity/entity-audit/entity-audit.service.ts',
        'shared/entity/entity-audit/entity-audit-event.model.ts',
        'shared/entity/entity-audit/entity-audit-history.scss',
        'shared/entity/entity-audit/entity-audit-history-modal.component.html',
        'shared/entity/entity-audit/entity-audit-history-modal.component.ts',
        'shared/entity/entity-audit/entity-history-options.model.ts',
        //              DELETE
        'shared/entity/entity-delete/entity-delete-modal.component.html',
        'shared/entity/entity-delete/entity-delete-modal.component.ts',
        'shared/entity/entity-delete/entity-delete-options.model.ts',
        //              UPDATE
        'shared/entity/entity-update/entity-update-options.model.ts',
        //              VIEW
        'shared/entity/entity-view/entity-view-options.model.ts',
        'shared/entity/entity-view/entity-view-modal.component.html',
        'shared/entity/entity-view/entity-view-modal.component.ts',

        // sort
        'shared/sort/sort.directive.ts',
        'shared/sort/sort-by.directive.ts',
        'shared/sort/sort-icon.component.ts',

        // util
        'shared/util.ts',

        // shared modules
        'shared/shared.module.ts',
        'shared/shared-libs.module.ts',
      ]
    },
    {
      ...clientApplicationBlock,
      condition: generator => generator.enableTranslation,
      templates: [
        // language
        'shared/language/find-language-from-key.pipe.ts',
        'shared/language/translate.directive.ts',
        'shared/language/language/language.component.ts',
        'shared/language/language/language.component.html',
        'shared/language/language/language.component.scss',
      ]
    }
  ],
  angularCore: [
    {
      ...clientApplicationBlock,
      templates: [
        'config/authority.constants.ts',
        'config/event.constants.ts',
        'config/pagination.constants.ts',
        'config/datepicker-adapter.ts',
        'config/nz-icon.config.ts',
        'config/dayjs.ts',
        'core/util/request-util.ts',
      ]
    }
  ],
  angularI18n: [
    {
      ...clientApplicationMain,
      condition: generator => generator.languages.includes('en'),
        templates: [
          'i18n/en/global.json',
          'content/images/en.svg',
      ]
    },
    {
      ...clientApplicationMain,
      condition: generator => generator.languages.includes('ru'),
        templates: [
          'i18n/ru/global.json',
          'content/images/ru.svg',
        ]
    },
    {
      ...clientApplicationMain,
      condition: generator => generator.languages.includes('uz-Cyrl-uz'),
        templates: [
          'i18n/uz-Cyrl-uz/global.json',
          'content/images/uz-Cyrl-uz.svg',
        ]
    },
    {
      ...clientApplicationMain,
      condition: generator => generator.languages.includes('uz-Latn-uz'),
        templates: [
          'i18n/uz-Latn-uz/global.json',
          'content/images/uz-Latn-uz.svg',
      ]
    },
  ]
};

export async function writeFiles({ application }) {
  if (!application.clientFrameworkAngular) return;


  console.log("files working!!");


  await this.writeFiles({
    sections: files,
    async: true,
    context: {
      ...application
    }
  });
}
