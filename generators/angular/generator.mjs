import { writeFiles } from './files-angular.mjs';
import { writeEntitiesFiles } from './entity-files-angular.mjs';
import { addEnumerationFiles } from './add-enumeration.mjs';
import { GeneratorBaseEntities } from 'generator-jhipster';
import {
  DEFAULT_PRIORITY,
  END_PRIORITY, INITIALIZING_PRIORITY,
  POST_WRITING_PRIORITY,
  PREPARING_EACH_ENTITY_FIELD_PRIORITY,
  PREPARING_PRIORITY,
  PRIORITY_PREFIX,
  WRITING_ENTITIES_PRIORITY,
  WRITING_PRIORITY
} from "generator-jhipster/esm/priorities";
import { roleTest } from "./role.mjs";

export default class extends GeneratorBaseEntities {
  packages;
  role;

  constructor(args, opts, features) {
    super(args, opts, { taskPrefix: PRIORITY_PREFIX, unique: 'namespace', ...features, sbsBlueprint: true });

    this.jhipsterOptions({
      executeBlueprint: {
        desc: 'Sub Blueprint',
        type: String
      }
    });

    this.sbsBlueprint = true;
  }

  async beforeQueue() {
    console.log('beforeQueue');
    await this.dependsOnJHipster('bootstrap-application');
    if (!this.delegateToBlueprint) {
      await this.dependsOnJHipster(`languages`);
    }
  }

  get [INITIALIZING_PRIORITY]() {
    return {
      async initializingTemplateTask() {
        this.role = await roleTest.call(this)
      }
    }
  }

  get [DEFAULT_PRIORITY]() {
    console.log('_default');
    return {
      async defaultPriority() {
        const entities = this.sharedData.getEntities().map(({ entity }) => entity);
        this.localEntities = entities.filter(entity => !entity.builtIn && !entity.skipClient);
      }
    };
  }

  get [PREPARING_PRIORITY]() {
    console.log('_preparing');
    return {
      async preparingPriority() {
        const {
          dependencies
        } = this.fs.readJSON(this.templatePath('../resources/package.json'));
        this.packages = dependencies;
      }
    };
  }

  get [PREPARING_EACH_ENTITY_FIELD_PRIORITY]() {
    console.log('_preparingEachEntityField');
    return {
      async changeTsTypeIfDayjs({ application, field }) {
        if (field.tsType === 'dayjs.Dayjs') {
          field.tsType = 'Date';
        }
      }
    };
  }

  get [POST_WRITING_PRIORITY]() {
    console.log('_postWriting');
    return {
      async postWritingTemplateTask() {
        this.packageJson.merge({
          dependencies: this.packages
        });
      }
    };
  }

  get [WRITING_PRIORITY]() {
    console.log('_writing');
    return {
      async writingTask(data) {
        await writeFiles.call(this, data);
      }
    };
  }

  get [WRITING_ENTITIES_PRIORITY]() {
    console.log('_writingEntities');
    return {
      async writingEntitiesTask({ application, entities, control }) {
        await writeEntitiesFiles.call(this, { application, entities, control });
        for (const entity of entities) {
          await addEnumerationFiles.call(this, { application, entity });
        }
      }
    };
  }

  get [END_PRIORITY]() {
    return {
      async endTemplateTask() {
        console.log(`Project successfully edited! 🎉`);
      }
    };
  }

  getTranslationKey(frontName, entityName) {
    return frontName + "." + entityName
  }

  isRoleHard() {
    return this.role === "hard"
  }

  getRoleType(crud_type, entityName, create = false) {
    switch (crud_type) {
      case "create": {
        if (this.isRoleHard()) {
          return entityName.toUpperCase() + "_C"
        } else {
          if (create)
            return "ROLE_" + entityName.toUpperCase() + "_CREATE"
          return entityName.toUpperCase() + "_CREATE"
        }
      }
      case "update": {
        if (this.isRoleHard()) {
          return entityName.toUpperCase() + "_U"
        } else {
          if (create)
            return "ROLE_" + entityName.toUpperCase() + "_UPDATE"
          return entityName.toUpperCase() + "_UPDATE"
        }
      }
      case "delete": {
        if (this.isRoleHard()) {
          return entityName.toUpperCase() + "_D"
        } else {
          if (create)
            return "ROLE_" + entityName.toUpperCase() + "_DELETE"
          return entityName.toUpperCase() + "_DELETE"
        }
      }
      case "read": {
        if (this.isRoleHard()) {
          return entityName.toUpperCase() + "_R"
        } else {
          if (create)
            return "ROLE_" + entityName.toUpperCase() + "_READ"
          return entityName.toUpperCase() + "_READ"
        }
      }
    }
  }
}
