import { JHIPSTER_JSON } from "./dir.mjs";

export async function roleTest() {
  let entityJsonFile = this.fs.read(JHIPSTER_JSON);
  const devopsObject = entityJsonFile.match(/"devops": \{([\s\S]*?)\}/)
  if (devopsObject && devopsObject[1]) {
    return JSON.parse("{" + devopsObject[1] + "}")['role']
  }
  const role = await this.prompt([
    {
      type: 'list',
      name: 'executeBlueprint',
      message: 'Which blueprint do you want to use?',
      choices: [
        {
          name: "User roles => ROLE_ENTITY_CREATE",
          value: "easy"
        },
        {
          name: "User roles => ENTITY_C",
          value: "hard"
        }
      ]
    }
  ]);
  const entity = `"applicationType": "gateway",`;
  const devopsObjectAndName = entity + `\n  "devops": {
    "role": "${role.executeBlueprint}"
  },`
  entityJsonFile = entityJsonFile.replace(entity, devopsObjectAndName);
  this.fs.write(JHIPSTER_JSON, entityJsonFile);
  return role.executeBlueprint
}
