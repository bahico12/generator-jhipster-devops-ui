/**
 * Copyright 2013-2023 the original author or authors from the JHipster project.
 *
 * This file is part of the JHipster project, see https://www.jhipster.tech/
 * for more information.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// This file is copied, types are moved and converted is converted to mjs, and `this` param of writeEntitiesFiles is removed
import { clientApplicationBlock, clientApplicationMain } from '../client/utils.mjs';
import { JHIPSTER_ENTITIES } from './dir.mjs';

export const angularFiles = {
  client: [
    {
      ...clientApplicationBlock,
      templates: ['layouts/navbar/navbar.component.html', 'entities/_entityFolder/_entityFile.model.ts']
    },
    {
      condition: generator => !generator.embedded,
      ...clientApplicationBlock,
      templates: [
        'entities/_entityFolder/_entityFile.routes.ts',
        'entities/_entityFolder/_entityFile.module.ts',
        'entities/_entityFolder/list/_entityFile.component.html',
        'entities/_entityFolder/list/_entityFile.component.ts',
        'entities/_entityFolder/service/_entityFile.service.ts',
        'entities/entity-routing.module.ts'
      ]
    },
    {
      condition: generator => !generator.readOnly && !generator.embedded,
      ...clientApplicationBlock,
      templates: [
        'entities/_entityFolder/update/_entityFile-update.component.html',
        'entities/_entityFolder/update/_entityFile-update.component.ts'
      ]
    },
    {
      ...clientApplicationMain,
      condition: generator => generator.languages.includes('en'),
      templates: [
        'i18n/en/_entityFile.json'
      ]
    },
    {
      ...clientApplicationMain,
      condition: generator => generator.languages.includes('ru'),
      templates: [
        'i18n/ru/_entityFile.json'
      ]
    },
    {
      ...clientApplicationMain,
      condition: generator => generator.languages.includes('uz-Cyrl-uz'),
      templates: [
        'i18n/uz-Cyrl-uz/_entityFile.json'
      ]
    },
    {
      ...clientApplicationMain,
      condition: generator => generator.languages.includes('uz-Latn-uz'),
      templates: [
        'i18n/uz-Latn-uz/_entityFile.json'
      ]
    }
  ]
};

export async function writeEntitiesFiles({ application, entities }) {
  // if (!application.clientFrameworkAngular) return;

  for (const entity of entities.filter(entity => !entity.skipClient && !entity.builtIn)) {

    await entityWriteIsStable.call(this, entity, async function() {
      await this.writeFiles({
        async: true,
        sections: angularFiles,
        context: {
          ...application,
          ...entity,
          entity,
          entities,
          microserviceAppName: entity.microserviceAppName,
        }
      });
    })
  }
}

async function entityWriteIsStable(entity, calc) {
  const entityPath = JHIPSTER_ENTITIES + entity.name + ".json";
  let entityJsonFile = this.fs.read(entityPath);

  const devopsObject = entityJsonFile.match(/"devops": \{([\s\S]*?)\}/)
  if (devopsObject && devopsObject[1]) {
    const generated = JSON.parse("{" + devopsObject[1] + "}")['generated']
    if (generated) {
      const againGenerate = await this.prompt([
        {
          type: 'list',
          name: 'executeBlueprint',
          message: `Regenerate of ${entity.name}?`,
          choices: [
            {
              name: 'Yes',
              value: true
            },
            {
              name: 'No',
              value: false
            },
          ]
        }
      ]);
      if (againGenerate['executeBlueprint']) {
        calc.call(this);
      }
    }
    calc.call(this);
  } else {
    await calc.call(this);
    await writeDevopsJson.call(this, entity, entityJsonFile, entityPath)
  }
}

async function writeDevopsJson(entity, entityJsonFile, entityPath) {
  const entityName = `"name": "${entity.name}",`;
  const devopsObjectAndName = entityName + `\n  "devops": {
    "generated": true
  },`
  entityJsonFile = entityJsonFile.replace(entityName, devopsObjectAndName);

  this.fs.write(entityPath, entityJsonFile);
}
