import chalk from 'chalk';
import BaseGenerator from 'generator-jhipster/esm/generators/base';
import {
 PRIORITY_PREFIX,
 INITIALIZING_PRIORITY,
 PROMPTING_PRIORITY,
 WRITING_PRIORITY,
 END_PRIORITY,
} from 'generator-jhipster/esm/priorities';

export default class extends BaseGenerator {
 constructor(args, opts, features) {
  super(args, opts, { taskPrefix: PRIORITY_PREFIX, ...features });

  if (this.options.help) return;
 }

 get [INITIALIZING_PRIORITY]() {
  return {
   async initializingTemplateTask() {},
  };
 }

 get [PROMPTING_PRIORITY]() {
  return {
   async promptingTemplateTask() {},
  };
 }

 get [WRITING_PRIORITY]() {
  return {
   async writingTemplateTask() {
    await this.writeFiles({
     sections: {
      files: [{ templates: ['template-file-base'] }],
     },
     context: this,
    });
   },
  };
 }

 get [END_PRIORITY]() {
  return {
   async endTemplateTask() {},
  };
 }
}
