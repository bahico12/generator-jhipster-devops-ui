import chalk from 'chalk';
import BootstrapApplicationGenerator from 'generator-jhipster/esm/generators/bootstrap-application';
import { PRIORITY_PREFIX, INITIALIZING_PRIORITY, PROMPTING_PRIORITY, END_PRIORITY } from 'generator-jhipster/esm/priorities';

export default class extends BootstrapApplicationGenerator {
 constructor(args, opts, features) {
  super(args, opts, { taskPrefix: PRIORITY_PREFIX, ...features });

  if (this.options.help) return;

  if (!this.options.jhipsterContext) {
   throw new Error(
    `This is a JHipster blueprint and should be used only like ${chalk.yellow('jhipster --blueprints generator-devops-ui')}`
   );
  }

  this.sbsBlueprint = true;
 }

 get [INITIALIZING_PRIORITY]() {
  return {
   async initializingTemplateTask() {},
  };
 }

 get [PROMPTING_PRIORITY]() {
  return {
   async promptingTemplateTask() {},
  };
 }

 get [END_PRIORITY]() {
  return {
   async endTemplateTask() {},
  };
 }
}
