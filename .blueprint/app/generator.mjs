import chalk from 'chalk';
import AppGenerator from 'generator-jhipster/esm/generators/app';
import { PRIORITY_PREFIX, INITIALIZING_PRIORITY, END_PRIORITY } from 'generator-jhipster/esm/priorities';

export default class extends AppGenerator {
 constructor(args, opts, features) {
  super(args, opts, { taskPrefix: PRIORITY_PREFIX, ...features });

  if (this.options.help) return;

  if (!this.options.jhipsterContext) {
   throw new Error(`This is a JHipster blueprint and should be used only like ${chalk.yellow('jhipster --blueprints devops-ui')}`);
  }

  this.sbsBlueprint = true;
 }

 get [INITIALIZING_PRIORITY]() {
  return {
   async initializingTemplateTask() {},
  };
 }

 get [END_PRIORITY]() {
  return {
   async endTemplateTask() {},
  };
 }
}
